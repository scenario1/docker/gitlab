# Gitlab and gitlab-runner with Docker

## Requirement before running compose file

1. Hardening OS

2. Install docker 

3. Install docker-compose

4. Change and complate config files


## Installation

**Step1**: chnage **DOMAIN** on docker-comose file with your domain.

**Step2:** check compose file and Run all services

```bash
docker-compose config
docker-compose up -d
```
**Step3:** Check compose services and view all services logs

```bash
docker-compose ps
docker-compose logs -f
```

**Step4:** check and visit your domain service:

git.DOMAIN: gitlab dashboard

**Step5:** crate gitlab root password

**Step6:** register runner on gitlab 

To do this, first go to Admin Area and then on Overview, Runner section and get this registration runner token.

Then register runner using the following command

**NOTE:** change GITLAB_TOKEN and GITLAB_DOMAIN

```bash
docker exec -it runner \
  gitlab-runner register \
    --non-interactive \
    --registration-token GITLAB_TOKEN \
    --locked=false \
    --description docker-stable \
    --url GITLAB_DOMAIN \
    --executor docker \
    --docker-image docker:stable \
    --docker-volumes "/var/run/docker.sock:/var/run/docker.sock"
```

## License
[DockerMe.ir](https://dockerme.ir)
